<?php


function jquery_upgrade($version_base, $version_ini)
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();
	
	$addon = bab_getAddonInfosInstance('jquery');

	if (false === $functionalities->register('jquery', $addon->getPhpPath() . 'jquery.php')) {
		return false; 
	}

	return true;
}



function jquery_onDeleteAddon() {

	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();

	if (false === $functionalities->unregister('jquery')) {
		return false;
	}

	return true;
}

// function jquery_onPageRefreshed()
// {
// 	global $babBody;
// 	$addon = bab_getAddonInfosInstance('jquery');
// 	$baseScriptsPath = $addon->getPhpPath();
// 	$stylesheetFile = $baseScriptsPath.('css/jquery.lightbox.css');
// 	$babBody->addstylesheetFile($stylesheetFile);	
	
// 	// 	require_once dirname(__FILE__) . '/functions.php';
// 	$addon = bab_getAddonInfosInstance('jquery');
// 	$baseScriptsPath = $addon->getPhpPath();
// 	$javascriptFile = $baseScriptsPath.'js/jquery.lightbox.js';
// 	$babBody->addJavascriptFile($javascriptFile);

// }